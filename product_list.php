<?php session_start();
include 'connection.php';
if (isset($_POST['add'])) {
  header('Location:  index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="my_jquery_functions.js"></script>
  <link rel="stylesheet" href="products.css">
  <script src="http://code.jquery.com/color/jquery.color.plus-names-2.2.0.min.js" integrity="sha256-97quMNaesEFsrV6uexbIR3+D1V1+75CVDXuLBFQ07lA=" crossorigin="anonymous"></script>

  <title>Product list</title>

  <script>
    $(document).ready(function() {

// modify checking all checkbox

      /*    $('#checkAll').on('click', function() {
           if(this.checked) {
             $(':checkbox').each(function() {
               this.checked = true;
             });
           } else {
             $(':checkbox').each(function() {
               this.checked = false;
             });
           }
         }); */



      $('.delete').click(function() {
        $checkbox = $(':checkbox:checked');
        if ($checkbox.length > 0) {
          $product_id = [];
          $($checkbox).each(function(key) {
            $product_id[key] = $(this).val();
          });
        }

        $.post({
          url: 'ajax3.php',
          type: 'POST',
          data: {
            product_id: $product_id
          }
        })
      });

    });
  </script>

</head>

<body>

  <!-- add/massdelete of added product -->

  <div class="container jumbotron mt-5">
    <div class="container mt-3 d-flex">
      <h2 class="mr-auto">Product list</h2>

      <div class="deleted mr-5"></div>

      <form action="product_list.php" method="POST">
        <input type="submit" value="ADD" name="add" class="btn mr-2 btn-success">
        <input type="submit" class="btn btn-danger delete" name="delete" value="MASS DELETE">


    </div>
    <hr>


    <!-- Adding new products -->

    <!-- <label for="checkAll">Check all</label>
               <input type="checkbox" id="checkAll" > -->
                 
    <div class="lists">

<!-- php code to get the products -->
      <?php
      $selected = "SELECT * FROM dvd_table";
      $result = mysqli_query($conn, $selected);

      if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {

      ?>
          <div class="list_group">
            <input class="d-flex align-self-end checkbox" type="checkbox" value="<?php echo $row['product_id'] ?>" name="product[]">
            <div class="list-item">
              <tr>
                <td><?php echo "SKU: " . $row['productSku']; ?></td><br>
                <td><?php echo "Name: " . $row['productName']; ?></td><br>
                <td><?php echo "Price: $" . $row['productPrice']; ?></td><br>
                <td><?php echo "Size: " . $row['productSize']; ?></td><br>
                <td><?php echo "Weight: " . $row['bookWeight']; ?></td><br>
                <td><?php echo "Height: " . $row['furnitureHeight']; ?></td><br>
                <td><?php echo "Width: " . $row['furnitureWidth']; ?></td><br>
                <td><?php echo "Length: " . $row['furnitureLength']; ?></td><br>
              </tr>
            </div>
            </form>
          </div>
      <?php

        }
      }
      ?>
    </div>
</body>

</html>