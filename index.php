<?php session_start();

if (isset($_POST['view'])) {
    header('Location: product_list.php');
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="my_jquery_functions.js"></script>
    <link rel="stylesheet" href="products.css">
    <link rel="stylesheet" href="jQuery.css">
    <script src="http://code.jquery.com/color/jquery.color.plus-names-2.2.0.min.js" integrity="sha256-97quMNaesEFsrV6uexbIR3+D1V1+75CVDXuLBFQ07lA=" crossorigin="anonymous"></script>
    <title>Product add</title>
    <link rel="scandiweb" href="scandi.png">

    <script>
        $(document).ready(function() {

            // checking product type 

            $('.size').hide();
            $('.dimensions').hide();
            $('.weight').hide();
            $('.prod2').change(function() {


                if ($opt = $('.prod2 option:selected:nth-child(2)').html()) {

                    $('.size').show();
                    $('.dimensions').hide();
                    $('.weight').hide();
                    $('#sku').val('JVC200123');

                } else if ($opt = $('.prod2 option:selected:nth-child(3)').html()) {
                    $('.dimensions').show();
                    $('.size').hide();
                    $('.weight').hide();
                    $('#sku').val('GGWP0007');

                } else {
                    $('.weight').show();
                    $('.dimensions').hide();
                    $('.size').hide();
                    $('#sku').val('TR120555');
                }

            })


            //  delete checked products

            $('#save').click(function(event) {
                event.preventDefault();
                $.post("ajax1.php", {
                    sku: $('#sku').val(),
                    name: $('#name').val(),
                    price: $('#price').val(),
                    size: $('#size').val(),
                    weight: $('#weight').val(),
                    height: $('#height').val(),
                    width: $('#width').val(),
                    length: $('#length').val(),

                }, function(success) {
                    $('#success').html(success).fadeOut(2500);
                })
            });

            $('#cancel').click(function(event) {
                event.preventDefault();
                $.post("ajax2.php", function(success) {

                    $('#success').html(success).fadeOut(2500);

                })
            });

            // form validation

            $('.sku-error').hide();
            $('.name-error').hide();
            $('.price-error').hide();
            $('.size-error').hide();
            $('.weight-error').hide();
            $('.height-error').hide();
            $('.width-error').hide();
            $('.length-error').hide();

            /* var error_sku = false;
            var error_name = false;
            var error_price = false;
            var error_size = false;
            var error_weight = false;
            var error_height = false;
            var error_width = false;
            var error_length = false;
             */

            $('#sku').focusout(function() {
                check_sku();
            });

            $('#name').focusout(function() {
                check_name();
            });

            $('#price').focusout(function() {
                check_price();
            });

            $('#size').focusout(function() {
                check_size();
            });

            $('#weight').focusout(function() {
                check_weight();
            });

            $('#height').focusout(function() {
                check_height();
            });

            $('#width').focusout(function() {
                check_width();
            });

            $('#length').focusout(function() {
                check_length();
            });


            function check_sku() {
                var sku_length = $('#sku').val().length;

                if (sku_length <= 0) {
                    $('.sku-error').html("Please, choose one of the type below").css('color', 'red');
                    $('.sku-error').show();
                    /*  error_sku = true; */
                } else {
                    $('.sku-error').hide();
                }
            }


            function check_name() {
                var name_length = $('#name').val().length;

                if (name_length <= 0) {
                    $('.name-error').html("Please, enter required data").css('color', 'red');
                    $('.name-error').show();
                    /*  error_name = true; */
                } else {
                    $('.name-error').hide();
                }
            }

            function check_price() {
                var price_length = $('#price').val().length;

                if (price_length <= 0) {
                    $('.price-error').html("Please, enter required data").css('color', 'red');
                    $('.price-error').show();
                    /*  error_sku = true; */
                } else {
                    $('.price-error').hide();
                }
            }

            function check_size() {
                var size_length = $('#size').val().length;

                if (size_length <= 0) {
                    $('.size-error').html("Please, enter required data").css('color', 'red');
                    $('.size-error').show();
                    /*  error_size = true; */
                } else {
                    $('.size-error').hide();
                }
            }

            function check_weight() {
                var weight_length = $('#weight').val().length;

                if (weight_length <= 0) {
                    $('.weight-error').html("Please, enter required data").css('color', 'red');
                    $('.weight-error').show();
                    /* error_weight = true; */
                } else {
                    $('.weight-error').hide();
                }
            }

            function check_height() {
                var height_length = $('#height').val().length;

                if (height_length <= 0) {
                    $('.height-error').html("Please, enter required data").css('color', 'red');
                    $('.height-error').show();
                    /* error_height = true; */
                } else {
                    $('.height-error').hide();
                }
            }

            function check_width() {
                var width_length = $('#width').val().length;

                if (width_length <= 0) {
                    $('.width-error').html("Please, enter required data").css('color', 'red');
                    $('.width-error').show();
                    /*  error_width = true; */
                } else {
                    $('.width-error').hide();
                }
            }

            function check_length() {
                var length_length = $('#length').val().length;

                if (length_length <= 0) {
                    $('.length-error').html("Please, enter required data").css('color', 'red');
                    $('.length-error').show();
                    /* error_length = true; */
                } else {
                    $('.length-error').hide();
                }
            }

            /* $('#formValid').submit(function() {
                error_length = false;
                error_width == false;
                error_length == false;
                error_sku == false;
                error_name == false;
                error_price == false;

                check_sku();
                check_name();
                check_price();
                check_size();
                check_weight();
                check_height();
                check_width();
                check_length();
                if(error_length == false && error_width == false && error_length == false && error_sku == false && error_name == false && error_price == false) {
                    return true;
                } else {
                    return false;
                }
            }); */


        });
    </script>
</head>
<body>
    <!-- save/cancel added product -->

    <div class="container jumbotron my-5">
        <div class="container mt-3 d-flex">
            <h2 class="success mr-auto">Product add</h2>
            <h3 id='success'></h3>
            <form action="product_list.php" id="formValid" method="POST">
                <button class="btn btn-success" id="save" name="save">Save</button>
                <button class="btn btn-danger" id="cancel" name="cancel">Cancel</button>
                <button class="btn btn-warning" id="view" name="view">List view</button>
        </div>

        <hr>

        <!-- input data about products -->

        <div class="form-group my-2 ">
            <label for="sku" class="">SKU</label>
            <input type="text" name="sku" id="sku" autocomplete="off" value="" placeholder="Choose the type below" readonly class="form-control my-2">
            <span class="sku-error"></span><br>
        </div>

        <div class="form-group my-2">
            <label for="name" class="">Name</labe>
                <input type="text" name="name" id="name" autocomplete="off" value="" placeholder="Enter name" class=" form-control my-2">
                <span class="name-error"></span><br>
        </div>

        <div class="form-group my-2">
            <label for="price" class="">Price ($)</labe>
                <input type="number" name="price" id="price" autocomplete="off" value="" placeholder="Enter price" class="form-control my-2">
                <span class="price-error"></span><br>
        </div>


        <!-- input fields for switching type -->

        <div class="form-group my-2">
            <label name="prod1" name="prod1" class="">Type switcher</labe>
                <select name="products" id="prod1" class="prod2 form-control my-2">
                    <option disabled selected value="Type of products">Type of products</option>
                    <option id="optSize" name="dvd" value="size">DVD</option>
                    <option id="optDimen" name="furniture" value="weight">FURNITURE</option>
                    <option id="optWeight" name="book" value="dimensions">BOOK</option>
                </select>
        </div>



        <!-- switched type will be displayed div below -->
        <div class="added"></div>


        <!-- input fields for product size/height/width/length (below) -->

        <div class="size form-group my-2 ">
            <label for="size" class="my-2">Size (MB)</label>
            <input type="number" name="size" id="size" autocomplete="off" value="" placeholder="Enter size" class="form-control my-2">
            <span class="size-error"></span><br>
        </div>

        <div class="weight form-group my-2">
            <label for="weight" class="my-2">Weight (KG)</label>
            <input type="number" name="weight" id="weight" autocomplete="off" value="" placeholder="Enter weight" class="form-control my-2">
            <span class="weight-error"></span>
        </div>

        <div class="dimensions form-group my-2">
            <label for="height" class="my-2">Height (CM)</label>
            <input type="number" name="height" id="height" autocomplete="off" value="" placeholder="Enter height" class="form-control my-2">
            <span class="height-error"></span><br>

            <label for="width" class="my-2">Width (CM)</label>
            <input type="number" name="width" id="width" autocomplete="off" value="" placeholder="Enter width" class="form-control my-2">
            <span class="width-error"></span><br>

            <label for="length" class="my-2">Length (CM)</label>
            <input type="number" name="length" id="length" autocomplete="off" value="" placeholder="Enter length" class="form-control my-2">
            <span class="length-error"></span><br>
        </div>


        </form>
        <hr>



    </div>
    <hr>
    <div class="d-flex">
        <h5 class="mx-auto">ScandiWeb Test</h5>
    </div>



</body>

</html>